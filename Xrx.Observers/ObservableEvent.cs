﻿using Xrx.Observers.Abstractions;

namespace Xrx.Observers
{
    public class ObservableEvent : IObservableEvent
    {
        public string Type { get; set; }
        public object Data { get; set; }
        public ObservableEvent(string type)
        {
            Type = type;
        }
        public ObservableEvent(string type, object data)
        {
            Type = type;
            Data = data;
        }
    }
}