﻿using System;
namespace Xrx.Observers.Abstractions
{
    public abstract class AbstractValuesProvider<TValue> : Subject, IValuesProvider<TValue>
    {
        public abstract TValue GetValue(object valueId);
    }
}
