﻿using System.Collections.Generic;
using Xrx.Observers.Abstractions;

namespace Xrx.Observers
{
    public class Subject : ISubject
    {
        protected HashSet<IObserver> observers;

        public Subject()
        {
            observers = new HashSet<IObserver>();
        }
        public virtual void Clear()
        {
            observers.Clear();
        }

        public void NotifyObservers(IObservableEvent @event = null)
        {
            foreach(var observer in observers)
            {
                observer.Notify(@event);
            }
        }
        public virtual void Subscribe(IObserver observer)
        {
            if (observers.Contains(observer)) { return; }
            observers.Add(observer);
        }

        public virtual void Unsubscribe(IObserver observer)
        {
            if (!observers.Contains(observer)) { return; }
            observers.Remove(observer);
        }
    }
}
