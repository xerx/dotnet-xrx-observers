# dotnet-xrx-observers

## Version 2.0.0 
[![Nuget](https://img.shields.io/nuget/v/Xrx.Observers.svg?style=for-the-badge)](https://www.nuget.org/packages/Xrx.Observers/)

#### Simple observer

```c#
class MagazineSubscriber : AbstractObserver
{
    private readonly string name;

    public MagazineSubscriber(ISubject subject, string name) : base(subject)
    {
        this.name = name;
    }
    public override void Notify(IObservableEvent @event = null)
    {
        if(@event.Type == "newIssue")
        {
            Console.WriteLine($"{name} received the new issue.");
        }
        else if(@event.Type == "discount")
        {
            Console.WriteLine($"{name}'s been notified about the {@event.Data}% discount.");
        }
    }
}
class Magazine : Subject
{
    public void NewIssue()
    {
        ObservableEvent issueEvent = new ObservableEvent("newIssue");
        NotifyObservers(issueEvent);
    }
    public void Discount(int percentage)
    {
        ObservableEvent discountEvent = new ObservableEvent("discount", percentage);
        NotifyObservers(discountEvent);
    }
}
class MainClass
{
    public static void Main(string[] args)
    {
        Magazine magazine = new Magazine();
        MagazineSubscriber magSub1 = new MagazineSubscriber(magazine, "Rami");
        MagazineSubscriber magSub2 = new MagazineSubscriber(magazine, "Sam");
        magazine.NewIssue();
        magazine.Discount(25);
    }
}
```

#### Property observer implementing INotifyPropertyChanged
```c#
class LocalizationManager : AbstractValuesProvider<string>
{
    public static CultureInfo EN = new CultureInfo("en");
    public static CultureInfo EL = new CultureInfo("el");
    public static LocalizationManager Current { get; } = new LocalizationManager();

    private Dictionary<CultureInfo, Dictionary<string, string>> StringsByCulture;
    private CultureInfo CurrentCulture;

    public LocalizationManager()
    {
        CurrentCulture = EL;
        //Load the strings from some provider
        StringsByLocale = ResourcesProvider.LoadStrings();
    }
    public void SetCulture(CultureInfo culture)
    {
        if(CurrentCulture != culture)
        {
            CurrentCulture = culture;
            NotifyObservers(new ObservableEvent("cultureChanged"));
        }
    }
    public override string GetValue(object valueId)
    {
        return StringsByCulture[CurrentCulture][(string)valueId];
    }
}
class LocalizedString : PropertyObserver<string>
{
    public LocalizedString(object valueId) : base(valueId, LocalizationManager.Current) { }
}
class MainClass
{
    public static void Main(string[] args)
    {
        LocalizedString localizedString = new LocalizedString("a_word_id");
        //Outputs the greek value of the word 
        Console.WriteLine(localizedString.Value);
        localizedString.PropertyChanged += (s, e) =>
        {
            Console.WriteLine(localizedString.Value);
        };
        //All localized strings are notified and the one above
        //outputs the english value of the word
        LocalizationManager.Current.SetCulture(LocalizationManager.EN);
    }
}
```