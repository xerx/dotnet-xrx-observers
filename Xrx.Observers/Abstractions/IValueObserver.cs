﻿namespace Xrx.Observers.Abstractions
{
    public interface IValueObserver<TValue> : IObserver
    {
        TValue Value { get; }
    }
}
