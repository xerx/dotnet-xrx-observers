﻿using System;
namespace Xrx.Observers.Abstractions
{
    public abstract class AbstractObserver : IObserver
    {
        protected ISubject subject;

        protected AbstractObserver()
        {

        }
        protected AbstractObserver(ISubject subject)
        {
            this.subject = subject;
            this.subject?.Subscribe(this);
        }

        public virtual void SetSubject(ISubject subject)
        {
            if(this.subject != null)
            {
                this.subject.Unsubscribe(this);
            }
            this.subject = subject;
            this.subject?.Subscribe(this);
        }

        public void Dispose()
        {
            subject.Unsubscribe(this);
        }
        public abstract void Notify(IObservableEvent @event = null);
    }
}
