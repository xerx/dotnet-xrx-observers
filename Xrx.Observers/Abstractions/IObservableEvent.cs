﻿namespace Xrx.Observers.Abstractions
{
    public interface IObservableEvent
    {
        string Type { get; set; }
        object Data { get; set; }
    }
}
