﻿using System.ComponentModel;
using Xrx.Observers.Abstractions;

namespace Xrx.Observers
{
    public class PropertyObserver<TValue> : ValueObserver<TValue>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public PropertyObserver(object valueId, ISubject subject) : base(valueId, subject)
        {
            
        }
        public PropertyObserver(object valueId) : base(valueId)
        {
        }
        public override void Notify(IObservableEvent @event = null)
        {
            base.Notify(@event);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
        }
    }
}
