﻿namespace Xrx.Observers.Abstractions
{
    public interface IValuesProvider<TValue> : ISubject
    {
        TValue GetValue(object valueId);
    }
}
