﻿using System;
namespace Xrx.Observers.Abstractions
{
    public interface ISubject
    {
        void Subscribe(IObserver observer);
        void Unsubscribe(IObserver observer);
        void NotifyObservers(IObservableEvent @event = null);
        void Clear();
    }
}
