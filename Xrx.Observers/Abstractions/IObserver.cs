﻿using System;
namespace Xrx.Observers.Abstractions
{
    public interface IObserver
    {
        void SetSubject(ISubject subject);
        void Notify(IObservableEvent @event = null);
        void Dispose();
    }
}
