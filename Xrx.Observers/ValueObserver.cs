﻿using System;
using Xrx.Observers.Abstractions;
namespace Xrx.Observers
{
    public class ValueObserver<TValue> : AbstractObserver, IValueObserver<TValue>
    {
        public TValue Value { get; protected set; }
        protected IValuesProvider<TValue> provider;
        private readonly object valueId;

        public ValueObserver(object valueId, ISubject subject) : base(subject)
        {
            this.valueId = valueId;
            UpdateProvider();
        }
        public ValueObserver(object valueId)
        {
            this.valueId = valueId;
        }
        public override void SetSubject(ISubject subject)
        {
            if(subject != null && subject is IValuesProvider<TValue>)
            {
                base.SetSubject(subject);
                UpdateProvider();
            }
            else
            {
                throw new ArgumentException("Subject is invalid");
            }
        }
        private void UpdateProvider()
        {
            provider = subject as IValuesProvider<TValue>;
            Value = provider.GetValue(valueId);
        }
        public override void Notify(IObservableEvent @event = null)
        {
            Value = provider.GetValue(valueId);
        }
    }
}
